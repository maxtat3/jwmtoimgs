import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

/**
 * Process images: watermark overlay and resize image
 */
public class WM {

	public static final double JPG_QUALITY = 0.93;

	/**
	 * Make process of image. To destination dir save processed image.
	 *
	 * @param pathToSrcImg absolute path to source image
	 * @param pathToDestImg absolute path to image in destination directory. To this directory are copies processed images.
	 * @param pathToWm absolute path to watermark
	 * @param widthResolution width resolution in processed image
	 * @param opacity watermark opacity in processed image
	 * @return true - is process successful
	 */
	public boolean process(File pathToSrcImg, File pathToDestImg, File pathToWm, int widthResolution, float opacity) {
		try {
			Thumbnails.of(pathToSrcImg)
				.size(widthResolution, widthResolution)
				.watermark(Positions.CENTER, ImageIO.read(pathToWm), opacity)
				.outputQuality(JPG_QUALITY)
				.toFile(pathToDestImg);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return true;
	}


}
