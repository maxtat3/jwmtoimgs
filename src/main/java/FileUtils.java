import java.io.File;

/**
 * General work with files
 */
public class FileUtils {

	public static final String NEW_DIR = "processed";

	/**
	 * Get file name only without extension from absolute path.
	 *
	 * @param absPath absolute path to file
	 * @return file name
	 */
	public static String getFileName(File absPath) {
		String p = absPath.toString();
		return p.substring(p.lastIndexOf("/") + 1, p.lastIndexOf("."));
	}

	/**
	 * Get dir path only from absolute path to any file.
	 * This method removed file name from path and returned path only.
	 *
	 * @param absPathToFile absolute path to file
	 * @return path to dir where contained file
	 */
	public static String getDirPath(File absPathToFile) {
		String pf = absPathToFile.toString();
		return pf.substring(0, pf.lastIndexOf("/") + 1);
	}

	/**
	 * Created a new directory from given absolute path.
	 *
	 * @param absPthNewDir path to it create new dir
	 * @return <tt>true</tt> if directory successful created
	 */
	public static boolean createDir(File absPthNewDir) {
		if (!absPthNewDir.exists()) {
			return absPthNewDir.mkdir();
		}
		return false;
	}
}
