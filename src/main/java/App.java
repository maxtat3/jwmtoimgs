import org.im4java.core.CommandException;
import org.im4java.core.ConvertCmd;
import org.im4java.core.IMOperation;

import java.io.File;
import java.util.ArrayList;

/**
 * Starting point to run app.
 * Arguments:
 * -i - absolute dir path to source images. Images must be in jpg|jpeg format.
 * -w - absolute path to watermark. Must be in png format.
 * -r - resize resolution for output files in pixels. By default output files has same resolution as in input fies
 * -t - transparent of overlay watermark to each image. In a float value. Valid ranges is [0.1 ... 1.0].
 * 0.1 - maximum transparency.
 */
public class App {

	/**
	 * Temporary image name in convert processing tiff -> jpeg without any change.
	 */
	public static final String TMP_IMAGE = "~tmpImage.jpeg";

	/**
	 * Will be write output script to disk.
	 */
	public static final boolean isOutScript = false;

	/**
	 * Absolute paths to images.
	 */
	private static File[] images;

	/**
	 * Absolute path to watermark.
	 */
	private static File wmPath;

	/**
	 * Resolution of destination images.
	 */
	private static int imgResolution;

	/**
	 * Transparency of watermark overplayed to destination images.
	 */
	private static float wmAlpha;


	public static void main(String[] args) {
		if (args.length == 0) {
			showHelp();

		} else if (args.length == 4) {

			if (new File(args[0]).isDirectory()) {
				images = new File(args[0]).listFiles();
			} else {
				System.out.println("arg0 - is not directory.");
			}

			if (new File(args[1]).isFile()) {
				wmPath = new File(args[1]);
			} else {
				System.out.println("arg1 - is not a file.");
			}

			imgResolution = Integer.parseInt(args[2]);

			wmAlpha = Float.parseFloat(args[3]);
		}

		File newDir = new File(FileUtils.getDirPath(images[0]) + FileUtils.NEW_DIR);
		if (! FileUtils.createDir(newDir)) {
			System.out.println("WARNING: ");
			System.out.println("Did not be able to create of new dir [" + newDir + "]. It may be already is exist !");
			System.out.println("App closed.");
			System.exit(-1);
		}

		WM wm = new WM();
		IMOperation imgOp;
		ConvertCmd convert;
		File destImg;

		for (File img : images) {
			imgOp = new IMOperation();
			// src tiff image
			imgOp.addImage(img.toString());
			imgOp.quality(100d);
			// dest jpeg image (tmp)
			imgOp.addImage(TMP_IMAGE);
			convert = new ConvertCmd();
			try {
				if (isOutScript) convert.createScript("image_magic-im4java.sh", imgOp);
				convert.run(imgOp);
			} catch (CommandException ce) {
				ce.printStackTrace();
				ArrayList<String> cmdError = ce.getErrorText();
				for (String err : cmdError) {
					System.err.println(err);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			destImg = new File(FileUtils.getDirPath(img) + FileUtils.NEW_DIR + "/" + FileUtils.getFileName(img) + ".jpeg");
			wm.process(new File(TMP_IMAGE), destImg, wmPath, imgResolution, wmAlpha);
		}

	}

	public static void showHelp() {
		System.out.println("-------------------------");
		System.out.println("-   Watermark to images   -");
		System.out.println("-------------------------");
		System.out.println("This a tiny application for overlay watermarks for list images. " +
			"Also may be resized images. Written in java.");
	}
}
