# jWmToImgs
This a tiny app for overlay watermarks for list images. Also may be resized images. Written in java. Run from command line. Tested only in linux (Ubuntu).

## How to use 
**java -jar jWmToImgs.jar <imgs_dir> <wm> <width> <wm_transp>**     
where:    
**imgs_dir** - absolute path to source images dir;    
**wm** - absolute path to watermark file. Usually in png format;     
**width** - width of image resolution;      
**wm_transp** - watermark transparent coefficient in [0...1] range. 0 value may be applied for only resize image without watermark overly;      

For example:   
```txt
java -jar jWmToImgs.jar /media/max/557C-3115/DCIM/ wm-cp.png 700 0.5    
``` 

## Quick call app from Bash
You can also call app from the bash command line with minimum arguments. 
This can help if the same arguments are often used. 
To do this, you need to wrap the call to the whole command with arguments in a function and place it as alias in bash aliases.

**Warning. Be careful when editing configuration bash files !**

Open *.bash_aliases* and put in new line next function:
```bash
function jWmToImgs(){
	java -jar ~/jWmToImgs.jar "$1" ~/wm-cp.png 700 0.5
}
```
Restart bash:
```bash
. ~/.bashrc
```

In terminal print *jWmToImgs* and pass absolute path to source images and press Enter key.      
Example:
```txt
jWmToImgs /media/max/557C-3115/DCIM/
```

Description:    

* Assumed as *jWmToImgs.jar* placed in root home catalog;
* Assumed as watermark placed in root home catalog;
* jWmToImgs is a custom functions name. From this name you can call short from bash command line;
* $1 - argument pass from command line. In this case contained absolute path to source images;
* *.bash_aliases* file commonly placed in home directory, for example in */home/username/.bash_aliases* ;

## Dependencies 
Must be installed ImageMagic tool. In most common Ubuntu releases is already pre installed. To check it you can run the *convert* command.